Commento is an easy-to-use commenting system for mobile browsers that keeps track of which paragraph you are reading on the screen. Comments are stored separately based on which paragraph you are reading.

To try it, simply download the repository and open the index.html file in your browser (Safari works best).

To replicate the mobile experience, reduce your browser window so it is as thin as possible.